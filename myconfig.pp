package { 'python-pip':
  ensure  => installed,
}
package { 'pip':
  require  => Package['python-pip'],
  ensure   => latest,
  provider => 'pip',
}
package { 'flask':
  require  => Package['pip','python-pip'],
  provider => 'pip',
}
package { 'unzip':
  ensure  => installed,
}



exec{ 'sudo sh -c \'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list\'':
  path    => ['/usr/bin', '/usr/sbin','/bin','/opt/puppetlabs/bin',],
}
exec{ 'wget --quiet -O - http://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -':
  path    => ['/usr/bin', '/usr/sbin','/bin','/opt/puppetlabs/bin',],
}
exec{ 'sudo apt-get update':
  path    => ['/usr/bin', '/usr/sbin','/bin','/opt/puppetlabs/bin',],
}

package { 'postgresql':
  ensure  => installed,
}


class { 'postgresql::server':
}

postgresql::server::db { 'mydatabasename':
  user     => 'yahuiliu',
  password => postgresql::postgresql_password('yahuiliu', 'iadt2020'),
}

exec{ 'sudo sh -c \'echo "export db_user=yahuiliu" >> /home/ubuntu/.profile\'':
  path    => ['/usr/bin', '/usr/sbin','/bin','/opt/puppetlabs/bin',],
}
exec{ 'sudo sh -c \'echo "export db_password=iadt2020" >> /home/ubuntu/.profile\'':
  path    => ['/usr/bin', '/usr/sbin','/bin','/opt/puppetlabs/bin',],
}